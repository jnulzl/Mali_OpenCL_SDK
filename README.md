# Mali_OpenCL_SDK

#### 介绍
**Mali_OpenCL_SDK,Mali 嵌入式GPU开发。**



#### 安装教程

1.  虚拟机交叉编译环境：Ubuntu16.04：略。
2.  Android ARM工具链生成：
    - 从[这里下载NDK](https://dl.google.com/android/repository/android-ndk-r14b-linux-x86_64.zip)
    - 解压后运行：python3 ./build/tools/make_standalone_toolchain.py --arch arm --api 19 --package-dir ${toolchainDir}
    - 将生成的工具链文件解压，cd到解压后的bin目录，执行pwd，获取当前目录，记为PWD
3.  修改SDK根目录下platform.mk文件
    - 添加export PATH=$PATH:/path/to/PWD
    - 修改CC:=arm-linux-androideabi-g++   AR:=arm-linux-androideabi-ar
4.  cd 到samples下的任意一个例子文件夹下，执行make即可生成可执行文件
5.  将生成的可执行文件、.so、.cl文件通过adb push到开发板，修改文件权限，即可运行
